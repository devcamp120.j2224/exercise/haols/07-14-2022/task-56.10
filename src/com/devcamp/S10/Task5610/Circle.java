package com.devcamp.S10.Task5610;

public class Circle {
    private double radius = 1.0;
    final double pi = 3.14;

    public Circle() {
        super();
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getCircumference() {
        double circumference = 2 * pi * radius;
        return circumference;
    }

    public double getArea() {
        double area = pi * radius * radius;
        return area;
    }

    @Override
    public String toString() {
        return String.format("Circle: radius = %s", radius);
    }

}
