import com.devcamp.S10.Task5610.Circle;

public class App {
    public static void main(String[] args) throws Exception {
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(5.0);
        System.out.println(circle1.toString());
        System.out.println(circle2.toString());
        double area1 = circle1.getArea();
        double area2 = circle2.getArea();
        double Cir1 = circle1.getCircumference();
        double Cir2 = circle2.getCircumference();
        System.out.println("Cirle 1: Area = " + area1 + " Circumference = " + Cir1);
        System.out.println("Cirle 1: Area = " + area2 + " Circumference = " + Cir2);
    }
}
